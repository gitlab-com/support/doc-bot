const https = require('https');
const URL = require('url').URL;
const util = require('util')

const {SecretManagerServiceClient} = require('@google-cloud/secret-manager');
const client = new SecretManagerServiceClient();

exports.createDocIssue = async (req, res) => {
  if (req.method !== 'POST') {
    res.status(400).send('Must send a POST request');
  }

  const [version] = await client.accessSecretVersion({
    name: 'projects/696404988091/secrets/SupportDocBot/versions/latest',
  });
  const payload = version.payload.data.toString();

  const endpoint = new URL(process.env.URL);
  const {
    ticket_id,
    ticket_url,
    latest_comment,
    current_user_id,
    current_user_name,
    secret
  } = req.body;
  console.log(`Creating for ticket ${ticket_id}`);

  const delimiter =
      '////';  // Separates title from description when creating the new issue

  const title = latest_comment.split(delimiter)[0].replace('Docs: ', '');
  const description = `${latest_comment.split(delimiter)[1]}

**Ticket:** [${ticket_id}](https://${ticket_url})\n
**Agent:** ${current_user_name}
	`;

  const postData = {
    title,
    labels: 'documentation',
    assignee_ids: [current_user_id],
    description,
  };

  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'PRIVATE-TOKEN': payload,
    },
    host: endpoint.hostname,
    path: endpoint.pathname + endpoint.search,
  };

  if (secret && authenticate(secret)) {
    const gitlabRequest = https.request(options, (response) => {
      response.on('data', (data) => console.log(data.toString()));
      response.on('end', (res) => console.log('Finished.'));
    });

    gitlabRequest.on('error', (err) => console.error(err));

    gitlabRequest.write(JSON.stringify(postData));
    gitlabRequest.end();

    res.status(200).send('Doc issue created');
  } else {
    console.error(
        'Failed authentication! Full request object logged below:\n\n');
    console.error(
        'IP Address(es):', util.inspect(req.ip), util.inspect(req.ips));
    console.error('Body:', util.inspect(req.body));
    res.status(403).send('Authentication failed');
  }
};

const authenticate = async (zendeskSecret) => {
  // Get the encrypted text of the secret sent from ZenDesk
  const [secretData] = await client.accessSecretVersion({
    name: 'projects/696404988091/secrets/SupportDocBotSecret/versions/latest',
  });
  const docBotSecret = secretData.payload.data.toString();

  return docBotSecret === zendeskSecret;
}