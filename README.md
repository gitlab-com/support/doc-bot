# Doc-Bot [Deprecated]

This project is used to manage our Zendesk to docs flow as described in the [support
handbook](https://about.gitlab.com/handbook/support/workflows/improving-documentation.html).

The goal of this function is to support our docs-first mentality and make it easier
and more efficient to create documentation that doesn't already exist.

Feedback and suggestions welcome!

## Deprecation

This function is no longer used by GitLab support. The function is replaced with native Zendesk features: Triggers and Http Target. 
